"
" since you always forget - reload config file with ':so %' or path to config
"
"set nocompatible " still needed in nvim?
set background=dark
set termguicolors
syntax enable

set mouse=a
set modelines=5
set clipboard=unnamed

" decent defaults for most stuff
set tabstop=4 shiftwidth=4 softtabstop=4

"Show all sorts of good information
set number
set showmode
set showcmd
set ruler
"Searching options
set hlsearch
set incsearch
set ignorecase
set smartcase

let g:python3_host_prog='/usr/local/bin/python3'

" embedded syntax support e.g. lua in this file
"let g:vimsyn_embed == 'lPr' -- not working, should allow lua, Python, ruby

" plugins via https://github.com/junegunn/vim-plug
" Installation:
" vim:
" curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
"   https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
"
" neovim:
" curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
"   https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
"
" run :PlugInstall to load them after installing plug.vim
" run :PlugUpdate to do the obvious 

filetype plugin indent on

" plugbeginend
call plug#begin()
" Plug 'easymotion/vim-easymotion' " do <Leader><Leader>f<letter>
Plug 'justinmk/vim-sneak' " do s<char1><char2> it's very goooood
" Plug 'ms-jpq/chadtree', {'branch': 'chad', 'do': ':UpdateRemotePlugins'}
Plug 'ms-jpq/chadtree', {'branch': 'chad', 'do': 'python3 -m chadtree deps'}
"Plug 'romgrk/barbar.nvim'
Plug 'airblade/vim-gitgutter'

" vvv colorschemes
Plug 'NLKNguyen/papercolor-theme'
"Plug 'vim-scripts/wombat256.vim' " color name is wombat256mod
"Plug 'arcticicestudio/nord-vim'
Plug 'nanotech/jellybeans.vim'
"Plug 'ciaranm/inkpot'
"Plug 'davidstelter/inkpot'
Plug 'rksz/inkpot256'
Plug 'chriskempson/base16-vim'
Plug 'morhetz/gruvbox'
" ^^^ colorschemes

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-commentary' " comment/uncomment block with gc and more
Plug 'sirtaj/vim-openscad'
" Plug 'editorconfig/editorconfig-vim'
"Plug 'itchyny/lightline.vim'
Plug 'leafgarland/typescript-vim'
Plug 'maxmellon/vim-jsx-pretty'
Plug 'pangloss/vim-javascript'
" Plug 'tpope/vim-eunuch'
" Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'edkolev/tmuxline.vim'
Plug 'tpope/vim-markdown'
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app & yarn install'  }
Plug 'rust-lang/rust.vim'
Plug 'cespare/vim-toml'
" vvv rust lsp goodies https://sharksforarms.dev/posts/neovim-rust/
Plug 'neovim/nvim-lsp'

" Collection of common configurations for the Nvim LSP client
Plug 'neovim/nvim-lspconfig'
Plug 'folke/lsp-colors.nvim', {'branch': 'main'} " adds lsp support for all colorschemes

Plug 'jose-elias-alvarez/null-ls.nvim', {'branch': 'main'}
Plug 'jose-elias-alvarez/nvim-lsp-ts-utils', {'branch': 'main'}

" Extensions to built-in LSP, for example, providing type inlay hints
Plug 'tjdevries/lsp_extensions.nvim'

" Autocompletion framework for built-in LSP
Plug 'nvim-lua/completion-nvim'

" Diagnostic navigation and settings for built-in LSP
Plug 'nvim-lua/diagnostic-nvim'
" ^^^ rust lsp goodies https://sharksforarms.dev/posts/neovim-rust/

Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'Shougo/deoplete-lsp'
" Plug 'ervandew/supertab'
" Plug 'Chiel92/vim-autoformat'
Plug 'sainnhe/edge'  " good color
" Plug 'racer-rust/vim-racer'
" ^^^ rust goods
" vvv clojure
"Plug 'guns/vim-clojure-static'
"Plug 'guns/vim-sexp'
"Plug 'tpope/vim-sexp-mappings-for-regular-people'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
"Plug 'luochen1990/rainbow'
"Plug 'tpope/vim-salve'
"Plug 'tpope/vim-fireplace'
"Plug 'tpope/vim-projectionist'
"Plug 'tpope/vim-dispatch'
" ^^^ clojure
Plug 'kyazdani42/nvim-web-devicons'
Plug 'folke/trouble.nvim', {'branch': 'main'}
Plug 'liuchengxu/graphviz.vim'
" v platformio, c++ etc
Plug 'embear/vim-localvimrc'

Plug 'normen/vim-pio'
Plug 'coddingtonbear/neomake-platformio'
" ^ platformio, c++ etc
call plug#end()
" plugbeginend

" sneak config: adds visual label/highlight
let g:sneak#label = 1

" *** WARNING ***
" Disable lvimrc sandboxing
" Note that if you work on third-party projects, any code in those projects' .lvimrc files include
" could be executed outside the sandbox; if you disable the sandbox as described above, be sure to
" carefully examine the contents of unfamiliar paths when localvimrc asks you whether you'd like to
" source a new localvimrc configuration file!
let g:localvimrc_sandbox = 0



lua << EOF
  require("trouble").setup {
    -- your configuration comes here
    -- or leave it empty to use the default settings
    -- refer to the configuration section below
  }
EOF

" keybindings for trouble.nvim
nnoremap <leader>xx <cmd>TroubleToggle<cr>
nnoremap <leader>xw <cmd>TroubleToggle workspace_diagnostics<cr>
nnoremap <leader>xd <cmd>TroubleToggle document_diagnostics<cr>
nnoremap <leader>xq <cmd>TroubleToggle quickfix<cr>
nnoremap <leader>xl <cmd>TroubleToggle loclist<cr>
nnoremap gR <cmd>TroubleToggle lsp_references<cr>

set background=dark

nnoremap <leader>v <cmd>CHADopen<cr>

"let g:chadtree_settings = { "keymap": {"trash": []} }

"local chadtree_settings = {["keymap.trash"] = {}, ["keymap.tertiary"] = "t"}
" local chadtree_settings = {keymap = {tertiary = "t"}}
lua << EOF
local chadtree_settings = {["keymap.trash"] = {}, ["keymap.tertiary"] = {"t"}}
vim.api.nvim_set_var("chadtree_settings", chadtree_settings)
EOF

let g:airline_powerline_fonts = 1

" https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#rome
"lua << EOF
"require'lspconfig'.rome.setup{}
"EOF

" https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#tsserver
"lua << EOF
"require'lspconfig'.tsserver.setup{}
"EOF

" cmd = { "typescript-language-server", "--stdio", "--jsx" }
"}
"EOF

"lua << EOF
"local lspconfig = require("lspconfig")
"local buf_map = function(bufnr, mode, lhs, rhs, opts)
"    vim.api.nvim_buf_set_keymap(bufnr, mode, lhs, rhs, opts or {
"        silent = true,
"    })
"end
"lspconfig.tsserver.setup({
"    on_attach = function(client, bufnr)
"        client.resolved_capabilities.document_formatting = false
"        client.resolved_capabilities.document_range_formatting = false
"        local ts_utils = require("nvim-lsp-ts-utils")
"        ts_utils.setup({})
"        ts_utils.setup_client(client)
"        buf_map(bufnr, "n", "gs", ":TSLspOrganize<CR>")
"        buf_map(bufnr, "n", "gi", ":TSLspRenameFile<CR>")
"        buf_map(bufnr, "n", "go", ":TSLspImportAll<CR>")
"        on_attach(client, bufnr)
"    end,
"})
"EOF

" vvv rust plugin stuffs from https://dev.to/drmason13/configure-neovim-for-rust-development-1fjn
"
" setup rust_analyzer LSP (IDE features)
" lua require'nvim_lsp'.rust_analyzer.setup{}

" " Use LSP omni-completion in Rust files
" autocmd Filetype rust setlocal omnifunc=v:lua.vim.lsp.omnifunc

" " Enable deoplete autocompletion in Rust files
" let g:deoplete#enable_at_startup = 1

" " customise deoplete maximum candidate window length
" call deoplete#custom#source('_', 'max_menu_width', 80)

" " Press Tab to scroll _down_ a list of auto-completions
" let g:SuperTabDefaultCompletionType = "<c-n>"

" " rustfmt on write using autoformat
" autocmd BufWrite * :Autoformat

" nnoremap <leader>c :!cargo clippy
" ^^^ rust plugin stuffs from https://dev.to/drmason13/configure-neovim-for-rust-development-1fjn
" ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
"
" RUST setup via lsp and rust-analyzer... this is complex and cranky so far.
" Blogs on setup:
" https://seenaburns.com/vim-setup-for-rust/
" https://sharksforarms.dev/posts/neovim-rust/
" https://dev.to/drmason13/configure-neovim-for-rust-development-1fjn
"
" vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
" vvv rust lsp goodies https://sharksforarms.dev/posts/neovim-rust/
"
" Set completeopt to have a better completion experience
" :help completeopt
" menuone: popup even when there's only one match
" noinsert: Do not insert text until a selection is made
" noselect: Do not select, force user to select one from the menu
set completeopt=menuone,noinsert,noselect

" Avoid showing extra messages when using completion
set shortmess+=c

" Configure LSP
" https://github.com/neovim/nvim-lspconfig#rust_analyzer
"lua <<EOF
"
"-- nvim_lsp object
"local nvim_lsp = require'nvim_lsp'
"
"-- function to attach completion and diagnostics
"-- when setting up lsp
"local on_attach = function(client)
"require'completion'.on_attach(client)
"require'diagnostic'.on_attach(client)
"end
"
"-- Enable rust_analyzer
"nvim_lsp.rust_analyzer.setup({ on_attach=on_attach })
"
"EOF

" Trigger completion with <Tab>
inoremap <silent><expr> <TAB>
			\ pumvisible() ? "\<C-n>" :
			\ <SID>check_back_space() ? "\<TAB>" :
			\ completion#trigger_completion()

function! s:check_back_space() abort
	let col = col('.') - 1
	return !col || getline('.')[col - 1]  =~ '\s'
endfunction


" Code navigation shortcuts
nnoremap <silent> <c-]> <cmd>lua vim.lsp.buf.definition()<CR>
nnoremap <silent> K     <cmd>lua vim.lsp.buf.hover()<CR>
nnoremap <silent> gD    <cmd>lua vim.lsp.buf.implementation()<CR>
nnoremap <silent> <c-k> <cmd>lua vim.lsp.buf.signature_help()<CR>
nnoremap <silent> 1gD   <cmd>lua vim.lsp.buf.type_definition()<CR>
nnoremap <silent> gr    <cmd>lua vim.lsp.buf.references()<CR>
nnoremap <silent> g0    <cmd>lua vim.lsp.buf.document_symbol()<CR>
nnoremap <silent> gW    <cmd>lua vim.lsp.buf.workspace_symbol()<CR>
nnoremap <silent> gd    <cmd>lua vim.lsp.buf.declaration()<CR>
nnoremap <silent> ga    <cmd>lua vim.lsp.buf.code_action()<CR>

" Visualize diagnostics
let g:diagnostic_enable_virtual_text = 1
let g:diagnostic_trimmed_virtual_text = '40'
" Don't show diagnostics while in insert mode
let g:diagnostic_insert_delay = 1

" Set updatetime for CursorHold
" 300ms of no cursor movement to trigger CursorHold
set updatetime=300
" Show diagnostic popup on cursor hold
"autocmd CursorHold * lua vim.lsp.util.show_line_diagnostics()

" Goto previous/next diagnostic warning/error
nnoremap <silent> g[ <cmd>PrevDiagnosticCycle<cr>
nnoremap <silent> g] <cmd>NextDiagnosticCycle<cr>

" have a fixed column for the diagnostics to appear in
" this removes the jitter when warnings/errors flow in
set signcolumn=yes

" Enable type inlay hints
" autocmd CursorMoved,InsertLeave,BufEnter,BufWinEnter,TabEnter,BufWritePost *
"			\ lua require'lsp_extensions'.inlay_hints{ prefix = '', highlight = "Comment" }

nnoremap <Leader>T :lua require'lsp_extensions'.inlay_hints()
nnoremap <Leader>t :lua require'lsp_extensions'.inlay_hints{ only_current_line = true }

" ^^^ rust lsp goodies https://sharksforarms.dev/posts/neovim-rust/

" make :Gbrowse work - open current file in browser on our gitlab!
" let g:fugitive_gitlab_domains = ['https://prod-gitlab.sprinklr.com']

"let g:lightline = {
"			\ 'colorscheme': 'jellybeans',
"			\ }

" The configuration options should be placed before `colorscheme edge`.
let g:edge_style = 'aura'
let g:edge_enable_italic = 1
let g:edge_disable_italic_comment = 1
set background=dark
"colorscheme edge

let g:PaperColor_Theme_Options = {
  \   'theme': {
  \     'default.dark': {
  \       'transparent_background': 1
  \     }
  \   }
  \ }

"let g:inkpot_black_background = 1
"colorscheme inkpot256

"colorscheme base16-tomorrow-night-eighties
"colorscheme base16-papercolor-dark
"colorscheme base16-eighties
"colorscheme base16-materia
"colorscheme base16-chalk
"colorscheme base16-twilight

"colorscheme PaperColor

let g:gruvbox_contrast_dark = 'hard'
let g:gruvbox_guisp_fallback = 1
let g:gruvbox_improved_strings = 1
let g:gruvbox_improved_warnings = 1
let g:gruvbox_invert_selection = 0
let g:gruvbox_invert_signs = 0
let g:gruvbox_invert_tabline = 1
colorscheme gruvbox

let g:jellybeans_use_term_italics = 1
let g:jellybeans_overrides = {
			\    'background': { 'guibg': '000000' },
			\}
"colorscheme jellybeans

" for pangloss/vim-javascript
let g:javascript_plugin_jsdoc = 1

let g:vim_jsx_pretty_highlight_close_tag = 1
let g:vim_jsx_pretty_colorful_config = 1 " default 0

filetype plugin indent on

" NERDTree
" https://medium.com/@victormours/a-better-nerdtree-setup-3d3921abc0b9
" nnoremap <Leader>f :NERDTreeToggle<Enter>
" nnoremap <silent> <Leader>v :NERDTreeFind<CR>
let NERDTreeAutoDeleteBuffer = 1
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1

autocmd FileType javascript setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType javascript.jsx setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType typescript setlocal ts=2 sts=2 sw=2 expandtab

autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" tab nav with Shift-h and Shift-l
nnoremap H gT
nnoremap L gt

set background=dark
