# my .config

## Configurations

### neovim

[neovim](https://neovim.io/)

[nvim/](nvim/)

### kitty

[kitty terminal](https://sw.kovidgoyal.net/kitty/)

[kitty/](kitty/)

### ~/.config/

This stuff goes in ~/.config in accordance with the [XDG specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html)


